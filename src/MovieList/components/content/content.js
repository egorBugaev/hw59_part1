import React from 'react'
import List from "./List/List";


const Content = props => {
	return (
		<div className="content">
			<List name={props.movies} deleteItem={props.deleteItem} editItem={props.editItem} findItem={props.findItem}/>
		</div>
	)
};

export default Content;