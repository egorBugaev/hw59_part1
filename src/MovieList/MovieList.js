import React, {Component} from 'react'
import Top from "./components/Top/Top";
import Content from "./components/content/content";


class MovieList extends Component {

    state = {
        moviesList: [],
        name:'',
        pos:''
    };

    getItem = ()=> {
      const moviesList = JSON.parse(localStorage.getItem('moviesList'));
      this.setState({moviesList});
    };
    componentDidMount() {this.getItem()};


  changeName = (event) => {
        let name = this.state.name;
        name = event.target.value;
        this.setState({name});

    };

    addItem = () => {
        const moviesList = [...this.state.moviesList];
        let name = this.state.name;
        moviesList.push({
          name: name,
          id: Date.now(),
        });
        name = '';
        this.setState({moviesList,name});
      localStorage.setItem('moviesList',JSON.stringify(moviesList));

    };

    deleteItem = (id) => {
        const moviesList = [...this.state.moviesList];
        const index = moviesList.findIndex(p => p.id === id);
        moviesList.splice(index, 1);
        this.setState({moviesList});
      localStorage.setItem('moviesList',JSON.stringify(moviesList));
    };

  findItem = (id) => {
    const moviesList = [...this.state.moviesList];
    let pos = moviesList.findIndex(p => p.id === id);
    this.setState({pos});



  };
  editItem = (event) => {
    const moviesList = [...this.state.moviesList];
    moviesList[this.state.pos].name = event.target.value;
    this.setState({moviesList});
    localStorage.setItem('moviesList',JSON.stringify(moviesList));

  };

  shouldComponentUpdate(nextProps, nextState) {
    return nextState.moviesList !== this.state.moviesList ||
           nextState.name !== this.state.name||
           nextState.pos !== this.state.pos;
  }

    render() {
        return (
            <div className="list">
                <Top
                    changeName={(event) => this.changeName(event)}
                    addItem={() => this.addItem()}
                    name={this.state.name}
                />
                <Content
                    movies={this.state.moviesList}
                    deleteItem={this.deleteItem}
                    findItem={this.findItem}
                    editItem={this.editItem}
                />

            </div>
        )
    }

}

export default MovieList;